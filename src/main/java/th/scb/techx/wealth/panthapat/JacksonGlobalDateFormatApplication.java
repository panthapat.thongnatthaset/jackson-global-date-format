package th.scb.techx.wealth.panthapat;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JacksonGlobalDateFormatApplication {

    public static void main(String[] args) {
        SpringApplication.run(JacksonGlobalDateFormatApplication.class, args);
    }
}