package th.scb.techx.wealth.panthapat;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import th.scb.techx.wealth.panthapat.models.Dummy;

@RestController
public class DummyController {

    protected final Logger logger = LogManager.getLogger(this.getClass());

    @Autowired
    private ObjectMapper mapper;

    @GetMapping(value = "/dummies")
    public Dummy singleEvent() {
        final Dummy dummy = new Dummy();
        final Date now = new Date();
        dummy.setDate(now);
        dummy.setSecondDate(now);
        dummy.setLocalDate(LocalDate.now());
        dummy.setLocalDateTime(LocalDateTime.now());

        try {
            logger.info(mapper.writeValueAsString(dummy));
        } catch (JsonProcessingException e) {
            logger.error(e);
        }

        return dummy;
    }
}