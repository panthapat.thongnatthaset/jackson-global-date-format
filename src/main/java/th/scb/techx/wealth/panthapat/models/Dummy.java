package th.scb.techx.wealth.panthapat.models;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

public class Dummy {
    private static final String ISO8601_FORMAT = "yyyy/MM/dd'T'HH:mm:ss.SSS'Z'";

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = ISO8601_FORMAT)
    private Date date;

    private Date secondDate;

    private LocalDateTime localDateTime;
    private LocalDate localDate;

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Date getSecondDate() {
        return secondDate;
    }

    public void setSecondDate(Date secondDate) {
        this.secondDate = secondDate;
    }

    public LocalDateTime getLocalDateTime() {
        return localDateTime;
    }

    public void setLocalDateTime(LocalDateTime localDateTime) {
        this.localDateTime = localDateTime;
    }

    public LocalDate getLocalDate() {
        return localDate;
    }

    public void setLocalDate(LocalDate localDate) {
        this.localDate = localDate;
    }
  
}
